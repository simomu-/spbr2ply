# spbr2ply #
spbr2ply  

### 概要 ###

spbrからplyファイルを生成する  
現バージョンではspbrの点情報をダウンサンプリングなしでそのまま利用している  

### ビルド&インストール ###

$make  
$make install  

### アンインストール ###

$make uninstall 

### 使い方 ###

spbr2ply SPBR_FILE_NAME  
spbr2ply SPBR_FILE_NAME OUTPUT_FILE_NAME  
