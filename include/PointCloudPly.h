#ifndef POINT_CLOUD_PLY_H
#define POINT_CLOUD_PLY_H 

#include <PlyData.h>

class PointCloudPly : public PlyData {
protected:
	double boundingX0;
	double boundingY0;
	double boundingZ0;
	double boundingX1;
	double boundingY1;
	double boundingZ1;

public:
	PointCloudPly();
	~PointCloudPly() = default;

	void SetBoundingBox(double x0, double y0, double z0, double x1, double y1, double z1);
	void VoxelGlidFilter(double glid_size);
	
};

#endif