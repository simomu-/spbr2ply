#ifndef SPBR_H
#define SPBR_H 

#include <vector>
#include <string>
#include <PointCloudPly.h>

class Spbr{
protected:
	double boundingX0;
	double boundingY0;
	double boundingZ0;
	double boundingX1;
	double boundingY1;
	double boundingZ1;

	std::string fileName;
	unsigned int pointCount;

	std::vector<double> vertexes;
	std::vector<double> normals;
	std::vector<unsigned char> colors;

public:
	Spbr();
	Spbr(std::string fname);
	~Spbr() = default;

	void ReadSpbr(std::string fname);
	void ReadHeader(char buf[]);
	void SetPointCount(unsigned int count){ pointCount = count;};
	unsigned int GetPointCount() const { return pointCount; };
	PointCloudPly ConvertToPly();
	void WriteToPly(const std::string& fname);
};

#endif