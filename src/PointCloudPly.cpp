#include <PointCloudPly.h>
#include <iostream>
#include <cmath>
#include <vector>

PointCloudPly::PointCloudPly() : 
		boundingX0(0),
		boundingY0(0),
		boundingZ0(0),
		boundingX1(0),
		boundingY1(0),
		boundingZ1(0){

}

void PointCloudPly::SetBoundingBox(double x0, double y0, double z0, double x1, double y1, double z1){
	boundingX0 = x0;
	boundingY0 = y0;
	boundingZ0 = z0;
	boundingX1 = x1;
	boundingY1 = y1;
	boundingZ1 = z1;
}


void PointCloudPly::VoxelGlidFilter(double glid_size){

	// double dx = (boundingX1 - boundingX0) / glid_size;
	// double dy = (boundingY1 - boundingY0) / glid_size;
	// double dz = (boundingZ1 - boundingZ0) / glid_size;
	// double dd = std::min({dx, dy, dz});
	std::cout << "VoxelGlidFilter glid_size = " << glid_size << std::endl; 
	int cx = std::ceil((boundingX1 - boundingX0) / glid_size) + 1;
	int cy = std::ceil((boundingY1 - boundingY0) / glid_size) + 1;
	int cz = std::ceil((boundingZ1 - boundingZ0) / glid_size) + 1;
	std::cout << cx << std::endl;
	std::cout << cy << std::endl;
	std::cout << cz << std::endl;

	std::vector<int> pointCounts = std::vector<int>(cx * cy * cz, 0);
	std::vector<double> pos = std::vector<double>((cx * cy * cz) * 3, 0);
	std::vector<double> nor = std::vector<double>((cx * cy * cz) * 3, 0);
	std::vector<int> col = std::vector<int>((cx * cy * cz) * 3, 0);

	for(int i = 0; i < GetVertexCount(); i++){

		int index_x = std::floor((vertexes[i * 3 + 0] + std::fabs(boundingX0)) / glid_size);
		int index_y = std::floor((vertexes[i * 3 + 1] + std::fabs(boundingY0)) / glid_size);
		int index_z = std::floor((vertexes[i * 3 + 2] + std::fabs(boundingZ0)) / glid_size);
		int index = 3 * (index_x + (index_y * cx) + (index_z * cx * cy));
		// std::cout << index_x << std::endl;
		// std::cout << index_y << std::endl;
		// std::cout << index_z << std::endl;
		if(index >= pos.size()){
			std::cerr << "Error" << std::endl;
			std::cout << pos.size() << std::endl;
			std::cout << index << std::endl;
			std::cout << index_x << std::endl;
			std::cout << index_y << std::endl;
			std::cout << index_z << std::endl;
		}
		// std::cout << std::endl;

		pos[index + 0] += vertexes[i * 3 + 0];
		pos[index + 1] += vertexes[i * 3 + 1];
		pos[index + 2] += vertexes[i * 3 + 2];

		nor[index + 0] += normals[i * 3 + 0];
		nor[index + 1] += normals[i * 3 + 1];
		nor[index + 2] += normals[i * 3 + 2];

		col[index + 0] += colors[i * 3 + 0];
		col[index + 1] += colors[i * 3 + 1];
		col[index + 2] += colors[i * 3 + 2];

		pointCounts[index_x + (cx * index_y) + (cx * cy * index_z)]++;

	}	
	std::cout << "Check" << std::endl;
	for(int i = 0; i < pointCounts.size(); i++){
		if(pointCounts[i] != 0){
			pos[i * 3 + 0] /= pointCounts[i];
			pos[i * 3 + 1] /= pointCounts[i];
			pos[i * 3 + 2] /= pointCounts[i];

			nor[i * 3 + 0] /= pointCounts[i];
			nor[i * 3 + 1] /= pointCounts[i];
			nor[i * 3 + 2] /= pointCounts[i];

			col[i * 3 + 0] /= pointCounts[i];
			col[i * 3 + 1] /= pointCounts[i];
			col[i * 3 + 2] /= pointCounts[i];			
		}

	}
	std::cout << "Check" << std::endl;

	// pos.erase(std::remove_if(pos.begin(), pos.end(), [](double v){ return v == 0; }), pos.end());
	// nor.erase(std::remove_if(nor.begin(), nor.end(), [](double v){ return v == 0; }), nor.end());
	// col.erase(std::remove_if(col.begin(), col.end(), [](double v){ return v == 0; }), col.end());

	vertexes.clear();
	vertexes.shrink_to_fit();
	normals.clear();
	normals.shrink_to_fit();
	colors.clear();
	colors.shrink_to_fit();

	std::cout << "vertex count " << pos.size() / 3 << std::endl;
	std::cout << "Check" << std::endl;

	vertexes = pos;
	normals = nor;
	colors = std::vector<unsigned char> (col.size());
	for(int i = 0; i < col.size(); i++){
		colors[i] = col[i];
	}
	// colors = col;

}