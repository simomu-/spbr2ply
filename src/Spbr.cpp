#include <Spbr.h>
#include <iostream>
#include <fstream>
#include <cstring>

Spbr::Spbr(){
}

Spbr::Spbr(std::string fname) : fileName(fname), pointCount(0){
	ReadSpbr(fname);
}

void Spbr::ReadSpbr(std::string fname){

	std::cout << fileName << " loading..." << std::endl;

	char buf[256];
	std::ifstream fout;
	fout.open(fname.c_str());
	if(!fout){
		std::cerr << "Error" << std::endl;
	}

	unsigned int count = 0;

	while(!fout.eof()){
		fout.getline(buf, sizeof(buf));
		if(buf[0] == '#'){
			if(buf[1] == '/'){
				ReadHeader(buf + 2);
			}
		}else{
			double x, y, z, nx, ny, nz;
			unsigned int r, g, b;
			sscanf(buf, "%lf %lf %lf %lf %lf %lf %u %u %u",
						 &x, &y, &z, 
						 &nx, &ny, &nz,
						 &r, &g, &b);

			vertexes[3 * count + 0] = x; 
			vertexes[3 * count + 1] = y; 
			vertexes[3 * count + 2] = z; 
			normals[3 * count + 0] = nx;
			normals[3 * count + 1] = ny;
			normals[3 * count + 2] = nz;
			colors[3 * count + 0] = r;
			colors[3 * count + 1] = g;
			colors[3 * count + 2] = b;

			count++;
			if(count > pointCount){
				break;
			}
		}
	}
	fout.close();
	std::cout << fileName << " load complete" << std::endl;

}

void Spbr::ReadHeader(char buf[]){
	if( !strncmp( buf, "NumParticles", strlen("NumParticles"))){
		unsigned int count;
		char tmp[16];
		sscanf(buf, "%s %u", tmp, &count);
		pointCount = count;
		vertexes = std::vector<double>(count * 3);
		normals = std::vector<double>(count * 3);
		colors = std::vector<unsigned char>(count * 3);
		std::cout << "NumParticles " << pointCount << std::endl;
	}

	if(!strncmp( buf, "BoundingBox", strlen("BoundingBox"))){
		char tmp[16];
		sscanf(buf, "%s %lf %lf %lf   %lf %lf %lf", 
					tmp, 
					&boundingX0, 
					&boundingY0,
					&boundingZ0,
					&boundingX1,
					&boundingY1,
					&boundingZ1);
		std::cout << "BoundingBox " << boundingX0 << " " << boundingY0 << " " << boundingZ0 << std::endl;
		std::cout << "            " << boundingX1 << " " << boundingY1 << " " << boundingZ1 << std::endl;
	}
}

PointCloudPly Spbr::ConvertToPly(){
	std::vector<int> f;
	PointCloudPly ply = PointCloudPly();
	ply.SetMesh(vertexes, f, normals, colors);
	ply.SetComment("Created by spbr2ply www.bitbucket.org/simomu-/spbr2ply");
	ply.SetBoundingBox(boundingX0, boundingY0, boundingZ0,
					   boundingX1, boundingY1, boundingZ1);
	// ply.VoxelGlidFilter(0.01);
	return ply;
}

void Spbr::WriteToPly(const std::string& fname){
	std::cout << "Convert to ply..." << std::endl;
	PlyData ply = ConvertToPly();
	ply.Write(fname);
	std::cout << "Convert complete" << std::endl;
}
