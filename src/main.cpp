#include <iostream>
#include <string>
#include <Spbr.h>
#include <FilePath.h>

int main(int argc, char const *argv[]){

	if(argc < 2){
		std::cerr << "spbr2ply SPBR_FILE_NAME" << std::endl;
		std::cerr << "spbr2ply SPBR_FILE_NAME OUTPUT_FILE_NAME" << std::endl;
		return 1;
	}

	Spbr spbr = Spbr(argv[1]);

	FilePath path;
	if(argc == 3){
		path.SetFilePath(argv[2]);
	}else{
		path.SetFilePath(argv[1]);
		path.ChangeExtension("ply");
	}

	spbr.WriteToPly(path.GetFilePath());
	std::cout << "Create ply file -> " << path.GetFilePath() << std::endl;
	return 0;
}